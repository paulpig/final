function [k2] = Step_0(path, name)

% path = 'D:\【MATLAB R2015B works】\LAB\1\1-2-6.bmp';
I=imread([path, name, '.BMP']);

%% 保证被处理图像为灰度图
flag = ndims(I); 
if flag==3
    I=rgb2gray(I);
end
k=I;
% figure(1)
% imshow(k)
%% 对比度增强
k=imadjust(k,[0.3 0.7],[0 1],4); 
% figure(2)
% imshow(k)
%% 孔洞填充
k1=imfill(k,'holes');
k1=(k1-k);

% for i = 1:1200
%     for j = 1:1600
%         if k1(i, j) ~= 0
%             k1(i, j) = 255;
%         end
%     end
% end

% figure(3)
% imshow(k1)
%% 二值化
level = graythresh(k1);  
k2 = im2bw(k1, level);
% figure(4)
% imshow(k1)
%% 图像处理：去毛刺 去孤立点 腐蚀
% k2=bwmorph(k1,'spur',inf);
% k2=bwmorph(k2,'clean',inf);
% k2=bwmorph(k2,'close',inf);
% figure(5)
% imshow(k2)
% k3=imfill(k2,'holes');
% k3=(k3-k2);
% figure(6)
% imshow(k3)
%% 找最大的孔洞
L = bwlabeln(k2, 8);
S = regionprops(L, 'Area');
S = ismember(L, find([S.Area] == max([S.Area])));
I1 = S;

k2=bwmorph(I1,'spur',inf);
k2=bwmorph(k2,'clean',inf);
k2=bwmorph(k2,'close',inf);

PATH = [path, 'holes-', name, '.tif'];
% figure(1),
% imshow(k2);
imwrite(k2, PATH);
