function [F] = Step_2_3(figure_st, st, type)

figure_f = zeros([360, type]);
F = zeros([360, type]);
standard_f = dct(st);

for i = 1 : type
    figure_f(:, i) = dct(figure_st(:, i));
    F(:, i) = figure_f(:, i) - standard_f;
%      F(:, i) = figure_st(:, i) - st;
%      F(:, i)=dct(F(:, i));
end


