% clear all;
load standard_st_4.mat;
load figure_st_4.mat;
%% 获取特征值（频谱），并量化
type = 10;
%
% st = zeros([360, 1]);
% I_t = false([1200, 1600]);
% path = 'E:\LAB-Photos\2017-01-15\holes-1-5-10-';
% for i = 1 : 10
%     I_t = imread([path, num2str(i), '.tif']);
%     st = st + Step_1_3(I_t);
% end
%
% st = st / 10;
% Figure_st(:, 10) = (Figure_st(:, 10) + st) / 2;

F = Step_2_3(Figure_st, Standard_st, type);

for i = 1 : type
    F(49 : 360, i) = 0;
    %     F(1, i) = 0;
end
for i=1:48
    F(i,:)=round(255.*(F(i,:)-Fmin(i))./(Fmax(i)-Fmin(i)));
end
% F = round(F ./ 2);
% 
% close all;
% for i = 1 : type
%     figure(23),
%     plot((1 : 360), F(:, i));
%     hold on
% end


%% 对所有的样本进行检验测试

dir = 'D:\yj\3D打印观察\2017-2-26-2\holes-4-5-';

Test_I = false([1200, 1600, 10, 10]);
Test_st = zeros([360, 10]);
Test_F = zeros([360, 10]);
D = zeros([10, 10]);

for i = 1 : 10
    for j = 1 : 10
        Test_I(:, :, i, j) = imread([dir, num2str(i), '-', num2str(j), '.tif']);
        temp_st = Step_1_3(Test_I(:, :, i, j));
        temp_st = temp_st / max(temp_st) * max(Standard_st);
        Test_st(:, i) = Test_st(:, i) + temp_st;    
    end
    Test_st(:, i) = Test_st(:, i) / 10;
    Test_F(:, i) = Step_2_3(Test_st(:, i), Standard_st, 1);
%     Test_F(:, i) = round(Test_F(:, i) ./ 2);
end
Test_F(49 : 360, :) = 0;
for k=1:48
    for l=1:10
        if Test_F(k,l)<=Fmin(k)
            Test_F(k,l)=0;
        elseif Test_F(k,l)>=Fmax(k)
            Test_F(k,l)=255;
        else
            Test_F(k,l)=round(255.*(Test_F(k,l)-Fmin(k))./(Fmax(k)-Fmin(k)));
        end
    end
end

for i = 1 : 10
    for j = 1 : 10
%         D(i, j) = mean(abs(Test_F(9 : 40, i) - F(9 : 40, j)))/ corr(Test_F(9 : 40, i), F(9 : 40, j)) * 2;
        D(i, j) = mean(abs(Test_F(1 : 48, i) - F(1 : 48, j)))/ corr(Test_F(1 : 48, i), F(1 : 48, j));

    end
end

for i = 1: 10
    for j = 1 : 10
        figure(24)
        plot((1:10), D(:, i));
        hold on
    end
end

% F_(:, :) = int8(F(1 : 48, :));
% 
% for i = 1 : 10
%     fid = fopen(['E:\LAB-data\figure1_1_', num2str(i), '.txt'], 'wb');
%     fwrite(fid, F_(:, i), 'int8');
%     fclose(fid);
% end

