#ifndef MYAPP_H
#define MYAPP_H

#include <QMainWindow>

namespace Ui {
class myApp;
}

class myApp : public QMainWindow
{
    Q_OBJECT

public:
    explicit myApp(QWidget *parent = 0);
    ~myApp();

private:
    Ui::myApp *ui;
};

#endif // MYAPP_H
