#include "mygamma.h"
using namespace cv;

void buildTable(unsigned char* table, int size, float gamma_)
{
    int i;
    for (i = 0; i < size; ++i)
    {
        *(table + i) = (unsigned char) (pow((i + 0.5f) / 256, gamma_) * 256.0 - 0.5f);
    }
}

void myGamma (Mat &src, float gamma)
{
    unsigned char table[256];
    int size = 256;

    buildTable(table, size, gamma);

    MatIterator_<unsigned char> it, end;
    for (it = src.begin<unsigned char>(), end = src.end<unsigned char>(); it != end; ++it)
    {
        *it = table[*it];
    }
}
