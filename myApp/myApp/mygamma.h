#ifndef MYGAMMA_H
#define MYGAMMA_H

#endif // MYGAMMA_H

#include "common.h"
#include <vector>

void buildTable (unsigned char* table, int size, float gamma_);

void myGamma (Mat &src, float gamma);
